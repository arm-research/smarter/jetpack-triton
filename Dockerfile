FROM nvcr.io/nvidia/l4t-base:r32.4.3

ENV DEBIAN_FRONTEND=noninteractive

COPY lib /triton/lib
COPY include /triton/include 
COPY bin /triton/bin
COPY custom-backend-sdk /triton/custom-backend-sdk
COPY model_repository /triton/model_repository

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        libgoogle-glog0v5 \
        libre2-dev \
        libssl-dev \
        libtool \
        libboost-dev \
        libcurl4-openssl-dev \
        zlib1g-dev \
	libb64-0d

CMD [ "bash" ]
